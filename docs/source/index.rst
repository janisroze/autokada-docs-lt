AUTOKADA CRM DOCUMENTATION
===========================

1. Home: Statistics and Status of Tasks
-------------------------------------------

.. image:: pics/1.PNG
   :scale: 80%

2. CRM -> Customers:  
-------------------------------

When pressing on a client name in the list below, Screen of the particular client will open

.. image:: pics/2.PNG
   :scale: 80%

3. CRM -> Debt
-------------------------------------------------

.. image:: pics/3.PNG
   :scale: 80%

4. Fleet -> Customer Fleet records
----------------------------------

.. image:: pics/4.PNG
   :scale: 80%

5. Summary about client
--------------------------------------

.. image:: pics/5.PNG
   :scale: 80%

5.1. Creating Contact Event
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/6.PNG
   :scale: 80%

5.2. Creating Reminder
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/7.PNG
   :scale: 40%

5.3. Adding Note
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/8.PNG
   :scale: 40%

5.4. Activity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/9.PNG
   :scale: 80%

5.5. Log
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/10.PNG
   :scale: 80%

5.6. Contacts
^^^^^^^^^^^^^

.. image:: pics/11.PNG
   :scale: 80%

5.7. Poll
^^^^^^^^^^^^^^^^^^^

.. image:: pics/12.PNG
   :scale: 40%

5.8. Invoices
^^^^^^^^^^^^^^

.. image:: pics/13.PNG
   :scale: 40%

5.9. Orders
^^^^^^^^^^^^^^

.. image:: pics/14.PNG
   :scale: 40%

5.10. Add New Contact
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/15.PNG
   :scale: 40%

6. Customer Debt
-------------------------------------------

.. image:: pics/16.PNG
   :scale: 80%

.. image:: pics/17.PNG
   :scale: 80%

7. Health icons explained
-------------------------------------------

.. image:: pics/18.PNG
   :scale: 80%

